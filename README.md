# Documentación en un proyecto Angular

## Pre-requisitos

### Node

En su ambiente de desarrollo debe estar instalado [Node.js](https://nodejs.org/es/download/) antes de continuar.

Para asegurarse que se encuentre instalado puede hacer uso del siguiente comando

```
> node -v
v6.11.3
```

### Angular CLI

Angular CLI requiere que se tenga instalado Node 6.9.0 o una versión superior, junto con NPM 3. Para instalar [Angular CLI](https://cli.angular.io/) utilizando el manejador de paquetes de [Node.js](https://nodejs.org/es/download/) (npm) debe ejecutar el siguiente comando:

```
> npm install -g @angular/cli
```

Por último para verificar la instalación puede hacer uso del siguiente comando:

```
> ng -v
    _                      _                 ____ _     ___
   / \   _ __   __ _ _   _| | __ _ _ __     / ___| |   |_ _|
  / △ \ | '_ \ / _` | | | | |/ _` | '__|   | |   | |    | |
 / ___ \| | | | (_| | |_| | | (_| | |      | |___| |___ | |
/_/   \_\_| |_|\__, |\__,_|_|\__,_|_|       \____|_____|___|
               |___/
@angular/cli: 1.4.2
node: 6.11.3
os: win32 x64
```

Una vez realizados estos pasos estaremos listos para empezar a desarrollar nuestra aplicación.

## 1 - Angular CLI

Angular CLI es una interfaz de comandos para Angular que nos permite automatizar varios procesos mediante simples comandos, como por ejemplo:

- Crear aplicaciones en Angular rápidamente.
- Ejecutar un servidor de desarrollo con LiveReload.
- Añadir features a tu aplicación de Angular.
- Realizar pruebas unitarias y end-to-end.
- Hace build de tu aplicación para producción.

Y algo muy importante es que ya tiene muchas buenas prácticas oficiales preconfiguradas, así que nos va a ayudar a mantener un estándar de código.


## 2 - Correr la aplicación
Instalar dependencias:

```
>$ npm install

```
Correr con:
```
>$ ng serve
```

## 3 - Agregar documentación

Una vez instalado el plugin, [DocumentThis](https://marketplace.visualstudio.com/items?itemName=joelday.docthis), oprimir ctrl + alt + d dos veces para que la herramienta genere la documentación automáticamente.

## 4 - Revisar coverage de documentación

Instalar compodoc:
```
sudo npm i --save-dev @compodoc/compodoc
```

Agregar en el package.json las siguiente definición:
```
"scripts": {
  .....// Al final de todo
   "compodoc": "compodoc -p src/tsconfig.app.json"
}
```

Para generar el reporte hacemos:
```
npm run compodoc
```